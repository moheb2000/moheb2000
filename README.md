## Hi there 👋

### About 😇

My name is **Mohammad Ebrahimi**. I like Linux and other free (as freedom) softwares. My linux distro is debian sid.

```
__________________
< Let's be friends >
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

### Technologies 🔧

![vue.js](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/vuejs.svg)
![express](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/express.svg)
![R](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/r.svg)
![R](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/vim.svg)
![linux](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/linux.svg)

### Donate 💰

[![Buy me a coffee](https://gitlab.com/moheb2000/moheb2000/-/raw/main/svg/buy-me-a-coffee.svg)](https://www.payping.ir/p/@moheb)


### More 📎

* 📬 Email: [ebrahimim79@duck.com](mailto://ebrahimim79@duck.com)
* 📢 Mastodon: [https://fosstodon.org/@moheb2000](https://fosstodon.org/@moheb2000)
* 🖊️ Matrix: [@moheb2000:wiiz.ir](https://matrix.to/#/@moheb2000:wiiz.ir)
